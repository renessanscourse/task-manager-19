package ru.ovechkin.tm.constant;

import org.jetbrains.annotations.NotNull;

@NotNull
public interface PathToSavedFile {

    @NotNull
    String DATA_BASE64 = "./data.base64";

    @NotNull
    String DATA_BIN = "./data.bin";

    @NotNull
    String DATA_JSON_JAXB = "./dataJaxb.json";

    @NotNull
    String DATA_JSON_MAPPER = "./dataMapper.json";

    @NotNull
    String DATA_XML_JAXB = "./dataJaxb.xml";

    @NotNull
    String DATA_XML_MAPPER = "./dataMapper.xml";

}
