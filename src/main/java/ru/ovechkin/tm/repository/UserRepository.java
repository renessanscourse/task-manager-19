package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.repository.IUserRepository;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.util.HashUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class UserRepository implements IUserRepository {

    @NotNull
    private List<User> users = new ArrayList<>();

//    {
//        users.add(new User("user", HashUtil.salt("user"), "use@mail.ck"));
//        users.add(new User("admin", HashUtil.salt("admin"), Role.ADMIN));
//        users.add(new User("admin1", HashUtil.salt("admin1"), Role.ADMIN));
//    }

    @NotNull
    @Override
    public List<User> findAll() {
        return users;
    }

    @NotNull
    @Override
    public User add(@NotNull final User user) {
        users.add(user);
        return user;
    }

    @Nullable
    @Override
    public User findById(@NotNull final String id) {
        for (@NotNull final User user : users) {
            if (id.equals(user.getId())) return user;
        }
        return null;
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        for (@NotNull final User user : users) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @NotNull
    @Override
    public User removeUser(@NotNull final User user) {
        users.remove(user);
        return user;
    }

    @Nullable
    @Override
    public User removeById(@NotNull final String id) {
        @Nullable final User user = findById(id);
        if (user == null) return null;
        return removeUser(user);
    }

    @Nullable
    @Override
    public User removeByLogin(@NotNull final String login) {
        final User user = findByLogin(login);
        if (user == null) return null;
        return removeUser(user);
    }

    @Nullable
    @Override
    public User merge(@NotNull final User user) {
        users.add(user);
        return user;
    }

    @Override
    public void merge(@Nullable final User... users) {
        if (users == null) return;
        for (@Nullable final User user : users) merge(user);
    }

    @Override
    public void merge(@Nullable final Collection<User> users) {
        if (users == null) return;
        for (final User user : users) merge(user);
    }

    @Override
    public void load(@Nullable final Collection<User> users) {
        clear();
        merge(users);
    }

    @Override
    public void load(@Nullable final User... users) {
        clear();
        merge(users);
    }

    @Override
    public void clear() {
        users.clear();
    }

}