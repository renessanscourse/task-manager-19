package ru.ovechkin.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.repository.IProjectRepository;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.exeption.unknown.IdUnknownException;
import ru.ovechkin.tm.exeption.unknown.IndexUnknownException;
import ru.ovechkin.tm.exeption.unknown.NameUnknownException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    @NotNull
    private List<Project> projects = new ArrayList<>();

    @Override
    public final void add(@NotNull final String userId,@NotNull final Project project) {
        project.setUserId(userId);
        projects.add(project);
    }

    @Override
    public final void remove(@NotNull final String userId,@NotNull final Project project) {
        @NotNull final List<Project> result = new ArrayList<>();
        for (final Project iterator: projects) {
            if (userId.equals(iterator.getUserId())) result.add(iterator);
        }
        result.remove(project);
    }

    @NotNull
    @Override
    public final List<Project> findUserProjects(@NotNull final String userId) {
        @NotNull final List<Project> result = new ArrayList<>();
        for (final Project project: projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @NotNull
    @Override
    public List<Project> findAllProjects() {
        return projects;
    }

    @Override
    public final void clear(@NotNull final String userId) {
        @NotNull final List<Project> projects = findUserProjects(userId);
        this.projects.removeAll(projects);
    }

    @NotNull
    @Override
    public Project findById(@NotNull final String userId,@NotNull final String id) {
        for (@NotNull final Project project: projects) {
            if (userId.equals(project.getUserId())) {
                if (id.equals(project.getId())) return project;
            }
        }
        throw new IdUnknownException();
    }

    @NotNull
    @Override
    public Project findByIndex(@NotNull final String userId,@NotNull final Integer index) {
        for (@NotNull final Project project: projects) {
            if (userId.equals(project.getUserId())) {
                if (projects.indexOf(project) == index) return project;
            }
        }
        throw new IndexUnknownException(index);
    }

    @NotNull
    @Override
    public Project findByName(@NotNull final String userId,@NotNull final String name) {
        for (@NotNull final Project project: projects) {
            if (userId.equals(project.getUserId())) {
                if (name.equals(project.getName())) return project;
            }
        }
        throw new NameUnknownException();
    }

    @NotNull
    @Override
    public Project removeById(@NotNull final String userId,@NotNull final String id) {
        @NotNull final Project project = findById(userId, id);
        projects.remove(project);
        return project;
    }

    @NotNull
    @Override
    public Project removeByIndex(@NotNull final String userId,@NotNull final Integer index) {
        @NotNull final Project project = findByIndex(userId, index);
        projects.remove(project);
        return project;
    }

    @NotNull
    @Override
    public Project removeByName(@NotNull final String userId,@NotNull final String name) {
        @NotNull final Project project = findByName(userId, name);
        projects.remove(project);
        return project;
    }

    @Override
    public void merge(@Nullable final Project... projects) {
        if (projects == null) return;
        for (@Nullable final Project project : projects) merge(project);
    }

    @Nullable
    @Override
    public Project merge(@Nullable final Project project) {
        if (project == null) return null;
        projects.add(project);
        return project;
    }

    @Override
    public void merge(@Nullable final Collection<Project> projects) {
        if (projects == null) return;
        for (@NotNull final Project project : projects) merge(project);
    }

    @Override
    public void load(@NotNull final Collection<Project> projects) {
        clear();
        merge(projects);
    }

    @Override
    public void load(@NotNull final Project... projects) {
        clear();
        merge(projects);
    }

    @Override
    public void clear() {
        projects.clear();
    }

}