package ru.ovechkin.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.util.TerminalUtil;

public class UserLockCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "lock-user";
    }

    @NotNull
    @Override
    public String description() {
        return "Lock user's account";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[LOCK USER]");
        System.out.print("ENTER USER'S LOGIN: ");
        @Nullable final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(userId, login);
        System.out.println("[OK]");
    }

    @NotNull
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMIN };
    }

}