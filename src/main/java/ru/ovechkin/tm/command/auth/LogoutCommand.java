package ru.ovechkin.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;

public class LogoutCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.LOGOUT;
    }

    @NotNull
    @Override
    public String description() {
        return "Logout from your account";
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().logout();
        System.out.println("[LOGOUT]");
        System.out.println("[OK]");
    }

}