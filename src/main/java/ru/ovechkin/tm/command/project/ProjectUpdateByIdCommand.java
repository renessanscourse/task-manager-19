package ru.ovechkin.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.util.TerminalUtil;

public final class ProjectUpdateByIdCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.PROJECT_UPDATE_BY_ID;
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by id";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[UPDATE PROJECT]");
        System.out.print("ENTER PROJECT ID: ");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Project project = serviceLocator.getProjectService().findProjectById(userId, id);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("ENTER NEW PROJECT NAME: ");
        @Nullable final String name = TerminalUtil.nextLine();
        System.out.print("ENTER NEW PROJECT DESCRIPTION: ");
        @Nullable final String description = TerminalUtil.nextLine();
        @NotNull final Project projectUpdated = serviceLocator.getProjectService().
                updateProjectById(userId, id, name, description);
        if (projectUpdated == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[COMPLETE]");
    }

}