package ru.ovechkin.tm.command;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.service.ServiceLocator;
import ru.ovechkin.tm.enumirated.Role;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public AbstractCommand() {
    }

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    public Role[] roles () {
        return null;
    }


    public abstract String arg();

    public abstract String name();

    public abstract String description();

    public abstract void execute() throws Exception;

}