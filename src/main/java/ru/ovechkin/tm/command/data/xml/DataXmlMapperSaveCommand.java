package ru.ovechkin.tm.command.data.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.constant.PathToSavedFile;
import ru.ovechkin.tm.dto.Domain;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.file.Files;

public class DataXmlMapperSaveCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.DATA_XML_MAPPER_SAVE;
    }

    @NotNull
    @Override
    public String description() {
        return "Save data to xml file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML SAVE]");

        @Nullable final Domain domain = new Domain();
        serviceLocator.getDomainService().save(domain);

        @Nullable final File file = new File(PathToSavedFile.DATA_XML_MAPPER);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @Nullable final ObjectMapper objectMapper = new XmlMapper();
        @Nullable final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);

        @Nullable final FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.close();

        System.out.println("[OK]");
    }

}