package ru.ovechkin.tm.command.data.base64;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.CmdConst;
import ru.ovechkin.tm.constant.PathToSavedFile;
import ru.ovechkin.tm.dto.Domain;
import sun.misc.BASE64Decoder;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class DataBase64LoadCommand extends AbstractCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.DATA_BASE64_LOAD;
    }

    @NotNull
    @Override
    public String description() {
        return "Load data from base64 file";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA BASE64 LOAD]");
        @Nullable final String base64date = new String(Files.readAllBytes(Paths.get(PathToSavedFile.DATA_BASE64)));
        @Nullable final byte[] decodedData = new BASE64Decoder().decodeBuffer(base64date);

        @Nullable final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodedData);
        @Nullable final ObjectInputStream objectInputStream  = new ObjectInputStream(byteArrayInputStream);
        @Nullable final Domain domain = (Domain) objectInputStream.readObject();
        serviceLocator.getDomainService().load(domain);
        objectInputStream.close();
        byteArrayInputStream.close();
        System.out.println("[OK]");
    }

}