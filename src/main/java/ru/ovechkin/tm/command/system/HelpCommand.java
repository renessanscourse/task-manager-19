package ru.ovechkin.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.bootstrap.Bootstrap;
import ru.ovechkin.tm.command.AbstractCommand;
import ru.ovechkin.tm.constant.ArgumentConst;
import ru.ovechkin.tm.constant.CmdConst;

import java.util.Collection;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String arg() {
        return ArgumentConst.ARG_HELP;
    }

    @NotNull
    @Override
    public String name() {
        return CmdConst.CMD_HELP;
    }

    @NotNull
    @Override
    public String description() {
        return "Display terminal commands";
    }

    @Override
    public void execute() {
        System.out.println("[HELP]");
        @NotNull final Bootstrap bootstrap = (Bootstrap) serviceLocator;
        @NotNull final Collection<AbstractCommand> commands = bootstrap.getCommands();
        for (@NotNull final AbstractCommand command : commands) {
            System.out.println(command.name()
                    + (command.arg() != null ? ", " + command.arg() + " - " : " - ")
                    + command.description());
        }
        System.out.println("[OK]");
    }

}
