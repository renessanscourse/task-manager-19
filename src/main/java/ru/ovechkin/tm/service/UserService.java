package ru.ovechkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.repository.IUserRepository;
import ru.ovechkin.tm.api.service.IUserService;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.exeption.empty.*;
import ru.ovechkin.tm.enumirated.Role;
import ru.ovechkin.tm.exeption.other.UserDoesNotExistException;
import ru.ovechkin.tm.exeption.unknown.IdUnknownException;
import ru.ovechkin.tm.exeption.unknown.LoginUnknownException;
import ru.ovechkin.tm.exeption.user.*;
import ru.ovechkin.tm.util.HashUtil;

import java.util.Collection;
import java.util.List;

public class UserService implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    public UserService(@NotNull IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @NotNull
    @Override
    public List<User> findAll() {
        @Nullable final List<User> users = userRepository.findAll();
        if (users == null) throw new UsersEmptyException();
        return users;
    }

    @NotNull
    @Override
    public User findById(@Nullable final String id) {
        findAll();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = userRepository.findById(id);
        if (user == null) throw new IdUnknownException();
        return user;
    }

    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) {
        findAll();
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = userRepository.findByLogin(login);
        if (user == null) throw new LoginUnknownException();
        return user;
    }

    @Nullable
    @Override
    public User removeUser(@Nullable final User user) {
        findAll();
        if (user == null) return null;
        return userRepository.removeUser(user);
    }

    @NotNull
    @Override
    public User removeById(@Nullable final String id) {
        findAll();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final User user = userRepository.findById(id);
        if (user == null) throw new UserDoesNotExistException();
        return userRepository.removeById(id);
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable final String userId, @Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = findByLogin(login);
        if (user == null) throw new LoginUnknownException();
        final User currentUser = findById(userId);
        if (currentUser == null) return null;
        if (user.getLogin().equals(currentUser.getLogin())) throw new SelfRemovingException();
        if (user.getRole() == Role.ADMIN) throw new AdminRemovingException();
        userRepository.removeByLogin(login);
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null) throw new EmailEmptyException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        final User user = create(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @NotNull
    @Override
    public User lockUserByLogin(@Nullable final String userid, @Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        if (user == null) return null;
        final User currentUser = findById(userid);
        if (currentUser == null) return null;
        if (user.getLogin().equals(currentUser.getLogin())) throw new SelfBlockingException();
        if (user.getRole() == Role.ADMIN) throw new AdminBlockingException();
        user.setLocked(true);
        return user;
    }

    @NotNull
    @Override
    public User unLockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        final User user = findByLogin(login);
        if (user == null) return null;
        user.setLocked(false);
        return user;
    }

    @NotNull
    @Override
    public User mergeOne(@Nullable final User user) {
        if (user == null) return null;
        userRepository.add(user);
        return user;
    }

    @Override
    public void merge(@NotNull final User... users) {
        for (@NotNull final User user : users) userRepository.merge(user);
    }

    @Override
    public void merge(@NotNull final Collection<User> tasks) {
        for (@NotNull final User user : tasks) userRepository.merge(user);
    }

    @Override
    public void load(@NotNull final Collection<User> tasks) {
        clear();
        userRepository.merge(tasks);
    }

    @Override
    public void load(@NotNull final User... tasks) {
        clear();
        userRepository.merge(tasks);
    }

    @Override
    public void clear() {
        userRepository.clear();
    }

}