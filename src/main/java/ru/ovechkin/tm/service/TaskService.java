package ru.ovechkin.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.api.repository.ITaskRepository;
import ru.ovechkin.tm.api.service.ITaskService;
import ru.ovechkin.tm.exeption.empty.*;
import ru.ovechkin.tm.entity.Task;

import java.util.Collection;
import java.util.List;

public class TaskService implements ITaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void create(@Nullable final String userId, @Nullable  final String name) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        final Task task = new Task();
        task.setName(name);
        taskRepository.add(userId, task);
    }

    @Override
    public void create(@Nullable final String userId, @Nullable  final String name, @Nullable  final String description) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        taskRepository.add(userId, task);
    }

    @Override
    public void add(@Nullable final String userId, @Nullable  final Task task) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (task == null) return;
        taskRepository.add(userId, task);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable  final Task task) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (task == null) return;
        taskRepository.remove(userId, task);
    }

    @Nullable
    @Override
    public List<Task> findUserTasks(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        return taskRepository.findAllUserTask(userId);
    }

    @Nullable
    public List<Task> findAllTasks() {
        return taskRepository.findAllTasks();
    }

    @Override
    public void removeTask(@Nullable final String userId, @Nullable  final Task task) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        taskRepository.remove(userId, task);
    }

    @Override
    public void removeAllTasks(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        taskRepository.clear(userId);
    }

    @Override
    public Task findTaskById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.findById(userId, id);
    }

    @Nullable
    @Override
    public Task findTaskByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (index == null || index < 0) throw new IndexEmptyException();
        return taskRepository.findByIndex(userId, index);
    }

    @Nullable
    @Override
    public Task findTaskByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return taskRepository.findByName(userId, name);
    }

    @Nullable
    @Override
    public Task updateTaskById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Task task = findTaskById(userId, id);
        if (task == null) return null;
        task.setId(id);
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Nullable
    @Override
    public Task updateTaskByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (index == null || index < 0) throw new IndexEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        final Task task = findTaskByIndex(userId, index);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Nullable
    @Override
    public Task removeTaskById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return taskRepository.removeById(userId, id);
    }

    @Nullable
    @Override
    public Task removeTaskByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (index == null || index < 0) throw new IndexEmptyException();
        return taskRepository.removeByIndex(userId, index);
    }

    @Nullable
    @Override
    public Task removeTaskByName(@Nullable final String userId, @Nullable final String name) {
        if (userId == null || userId.isEmpty()) throw new UserEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return taskRepository.removeByName(userId, name);
    }

    public void mergeArray(@Nullable final Task... tasks) {
        if (tasks == null) return;
        for (@Nullable final Task task : tasks) taskRepository.merge(task);
    }

    @Nullable
    public Task mergeOne(@Nullable final Task task) {
        if (task == null) return null;
        taskRepository.merge(task);
        return task;
    }

    public void mergeCollection(@NotNull final Collection<Task> tasks) {
        for (@NotNull final Task task : tasks) taskRepository.merge(task);
    }

    public void load(@Nullable final Collection<Task> tasks) {
        if (tasks == null || tasks.isEmpty()) return;
        clear();
        taskRepository.merge(tasks);
    }

    public void load(@Nullable final Task... tasks) {
        if (tasks == null) return;
        clear();
        taskRepository.merge(tasks);
    }

    public void clear() {
        taskRepository.clear();
    }

}