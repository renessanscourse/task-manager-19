package ru.ovechkin.tm.entity;

import org.jetbrains.annotations.NotNull;

import java.io.Serializable;

public class Project extends AbstractEntity implements Serializable {

    @NotNull
    private String name = "";

    @NotNull
    private String description = "";

    @NotNull
    private String userId;

    @NotNull
    public String getUserId() {
        return userId;
    }

    public void setUserId(@NotNull String userId) {
        this.userId = userId;
    }

    @NotNull
    public String getName() {
        return name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NotNull String description) {
        this.description = description;
    }

    @NotNull
    @Override
    public String toString() {
        return getId() + ": " + name;
    }

}