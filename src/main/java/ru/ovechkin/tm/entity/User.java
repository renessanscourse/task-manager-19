package ru.ovechkin.tm.entity;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.enumirated.Role;

import java.io.Serializable;

@NoArgsConstructor
public class User extends AbstractEntity implements Serializable {

    @NotNull
    private String login = "";

    @NotNull
    private String passwordHash = "";

    @NotNull
    private String email = "";

    @NotNull
    private String firstName = "";

    @NotNull
    private String lastName = "";

    @NotNull
    private String middleName = "";

    @NotNull
    private Role role = Role.USER;

    @NotNull
    private Boolean locked = false;

    @NotNull
    public Boolean getLocked() {
        return locked;
    }

    public void setLocked(@NotNull Boolean locked) {
        this.locked = locked;
    }

    @NotNull
    public Role getRole() {
        return role;
    }

    public void setRole(@NotNull Role role) {
        this.role = role;
    }

    @NotNull
    public String getLogin() {
        return login;
    }

    public void setLogin(@NotNull String login) {
        this.login = login;
    }

    @NotNull
    public String getEmail() {
        return email;
    }

    public void setEmail(@NotNull String email) {
        this.email = email;
    }

    @NotNull
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(@NotNull String firstName) {
        this.firstName = firstName;
    }

    @NotNull
    public String getLastName() {
        return lastName;
    }

    public void setLastName(@NotNull String lastName) {
        this.lastName = lastName;
    }

    @NotNull
    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(@NotNull String middleName) {
        this.middleName = middleName;
    }

    @NotNull
    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(@NotNull String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public User(@NotNull String login,@NotNull String passwordHash,@NotNull Role role) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.role = role;
    }

    public User(@NotNull String login,@NotNull String passwordHash,@NotNull String email) {
        this.login = login;
        this.passwordHash = passwordHash;
        this.email = email;
    }

}