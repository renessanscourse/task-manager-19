package ru.ovechkin.tm.dto;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.entity.Project;
import ru.ovechkin.tm.entity.Task;
import ru.ovechkin.tm.entity.User;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@XmlRootElement
@XmlAccessorType(XmlAccessType.PROPERTY)
public final class Domain implements Serializable {

    private List<Project> projects = new ArrayList<>();

    private List<Task> tasks = new ArrayList<>();

    private List<User> users = new ArrayList<>();

}