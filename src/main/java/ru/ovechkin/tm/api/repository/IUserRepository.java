package ru.ovechkin.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import ru.ovechkin.tm.entity.User;

import java.util.Collection;
import java.util.List;

public interface IUserRepository {

    @NotNull
    List<User> findAll();

    @NotNull
    User add(@NotNull User user);

    User findById(@NotNull String id);

    User findByLogin(@NotNull String login);

    @NotNull
    User removeUser(@NotNull User user);

    User removeById(@NotNull String id);

    User removeByLogin(@NotNull String login);

    User merge(@NotNull User user);

    void merge(@NotNull User... users);

    void merge(@NotNull Collection<User> tasks);

    void load(@NotNull Collection<User> tasks);

    void load(@NotNull User... tasks);

    void clear();

}