package ru.ovechkin.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    @Nullable
    List<User> findAll();

    @Nullable
    User create(String login, String password);

    @Nullable
    User create(String login, String password, String email);

    @Nullable
    User create(String login, String password, Role role);

    @Nullable
    User findById(String id);

    @Nullable
    User findByLogin(String login);

    User removeUser(User user);

    @Nullable
    User removeById(String id);

    @Nullable
    User removeByLogin(String userId, String login);

    @Nullable
    User lockUserByLogin(String userId, String login);

    @Nullable
    User unLockUserByLogin(String login);

    @Nullable
    User mergeOne(User user);

    void merge(@Nullable User... users);

    void merge(@Nullable Collection<User> tasks);

    void load(@Nullable Collection<User> tasks);

    void load(@Nullable User... tasks);

    void clear();

}