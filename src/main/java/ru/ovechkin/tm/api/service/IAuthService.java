package ru.ovechkin.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.ovechkin.tm.entity.User;
import ru.ovechkin.tm.enumirated.Role;

public interface IAuthService {

    @Nullable
    String getUserId();

    void checkRoles(@Nullable Role[] roles);

    boolean isAuth();

    void logout();

    void login(@Nullable String login, @Nullable String password);

    void registry(@Nullable String login, @Nullable String password, @Nullable String email);

    @Nullable
    User findUserByUserId(@Nullable String id);

    void updateProfileInfo(
            @Nullable String newLogin,
            @Nullable String newFirstName,
            @Nullable String newMiddleName,
            @Nullable String  newLastName,
            @Nullable String newEmail
    );

    void updatePassword(@Nullable String currentPassword, @Nullable String newPassword);

}